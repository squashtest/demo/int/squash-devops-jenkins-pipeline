# squash-devops-jenkins-pipeline

This demo project is part of the Squash DevOps - OpenTestFactory (OTF) integration demo. It aims at showcasing how a Jenkins pipeline can incorporate a QA step running a test plan defined in Squash TM. It must be empahsized that this is a working example.

## How Does it work ?

The key components are

- The [Jenkinsfile](https://www.jenkins.io/doc/book/pipeline/jenkinsfile/) that defines the pipeline with a single QA *step* within a single *stage*. This QA step runs an OpenTestFactory workflow against a OpenTestFactory orchestrator.
- The  [Open Test Factory Workflow](https://opentestfactory.org/specification/workflows.html)"workflow.yml"  that defines the test workflow to be ran. It uses the Squash [generator](https://autom-devops-en.doc.squashtest.com/2022-01/devops/callWithPeac.html#integration-of-the-squash-tm-execution-plan-retrieval-step-into-a-peac)  to retreive and execute a test plan defined in Squash TM.

The various secrets (Orchestrator Token, Squash TM credentials, ...) are meant to be dealt with the built-in Jenkins secrets management mechanisms.

## Pre-requisites

This project assumes

- A deployed Jenkins with the [OpenTestFactory Jenkins](https://gitlab.com/opentestfactory/jenkins-plugin) plugin installed.
  ![OpenTestFactory Jenkins](static/images/otf-jenkins-plugin.png)

- A deployed Squash Orchestrator. It must be accessible from the Jenkins agents. The Squash orchestrator is a wrapper around the OpenTestFactory orchestrator and is part of the [*Squash  DEVOPS* distribution](https://squashtest.gitlab.io/doc/squash-autom-devops-doc-fr/2022-01/devops/install.html#squash-orchestrator).

- A deployed Squash TM with the plugins from [*Squash  DEVOPS* distribution](https://squashtest.gitlab.io/doc/squash-autom-devops-doc-fr/2022-01/devops/install.html#plugins-squash-tm) installed.

- That an automated test plan is defined in Squash TM. How to create such a test plan is defined [here](https://autom-devops-en.doc.squashtest.com/2022-01/autom/pilotFromSquash.html).

Please be sure that those requirements are met otherwise this small tutorial is doomed to failure.

## How to use it

1. Clone or Fork this [project](https://gitlab.com/squashtest/demo/int/squash-devops-jenkins-pipeline) on your favorite SCM system. Here we use GitLab but any other SCM compatible with Jenkins and accesible from your Jenkins instance can be used.

2. In Jenkins
  
   a. Be sure that your Orchestrator is defined in the system configuration page, section *OpenTestFactory Orchestrator servers*, c.f. screen capture down below.
      ![Orchestrator definition in Jenkins](static/images/otf-server-config-jenkins.png)

      The credential is a valid (to the orchestrator) JWT Token. [Here](https://opentestfactory.org/tools/running-commands.html#generate-token-using-key) is a tutorial on how to generate such a token. It must be registered within Jenkins store as a "secret text".

    b. The OpenTestFactory [workflow](workflow.yml) uses environnement variables *SQUASH_USER* and *SQUASH_PASSWORD* to valuate the Squash TM crendentials used to retreive the test plan. These environnement variables are made available in the Jenkins pipeline execution context via the "withCredentials" clause in the [jenkinsfile](./Jenkinsfile). This clause will get a "Username and Password credential" stored in the Jenkins store and put it in the execution context of the pipeline. Please note that in the clause the variable are prefixed with*OPENTF_* in ordrer for the OpenTestFactory Jenkins plugin to be able to recogize it.
        ![Credentials as environemnt variables in the Jenkins pipeline](static/images/credentials-as-env-vars.png)

    Hence one must define in the Jenkins store the "tm-demo-int-user-automation-server" credentials in Jenkins. this corresponds to the login and pasword of the technical account (defined in Squash TM) used to fetch the test plan.
        ![Squash TM credentials stored in Jenkins](static/images/sqtm-credentials-in-jenkins.png)

   c. Create a new pipeline with the pipeline source pointing to your SCM project URL.
         ![Squash TM credentials stored in Jenkins](static/images/pipeline-definition.png)

    And that is it for the Jenkins configuration part !

3. In Squash TM retreive the unique identifie (UUID) of your test plan on the iteration info tab.
     ![Squash TM test plan unique identifier](static/images/sqtm-testplan-uuid.png)

4. In your favorit IDE modify the [workflow file](workflow.yml) to:

   a. point to the correct URL of your Squash TM instance.

   b. target the correct test plan, using the UUID you just retreived!

     ![Workflow modifications](static/images/workflow-modifications.png)

5. You are now ready to launch your pipeline. On you Jenkins job home page click the "launch build" button. Uppon completion of the exectuion and if everything wen't right you should see:

    a. In the Jenkins pipeline logs, that the test workflow execution was a success.
         ![Test workflow execution logs in jenkins](static/images/workflow-logs.png)

    b.In Squash TM, that the execution results and test reports were pushed.
             ![Execution results in Squash TM](static/images/execution-results.png)
